import google
from google.cloud import pubsub_v1
import os
import json
from bucket_operations import save_picture_for_converting_to_video, upload_video_to_bucket
from database_operations import add_links_for_video_to_cat, change_status, get_user_info, save_error
from picture_to_video_interface import convert_videos
from google.cloud import storage
from google.api_core import retry

os.environ[
    'GOOGLE_APPLICATION_CREDENTIALS'] = "SOME CREDENTIALS"

project_id = "PROJECT_ID"
topic_id = "TOPIC ID "
subscription_id = " ID of PubSubSubscription "
subscriber = pubsub_v1.SubscriberClient()

bucket_name = 'Backet for saving information'
storage_client = storage.Client()
bucket = storage_client.bucket(bucket_name)

subscription_path = subscriber.subscription_path(project_id, subscription_id)

NUM_MESSAGES = 1


def pull_messages():
    """Take one message from Pubsub for processing"""
    response = subscriber.pull(
            request={"subscription": subscription_path, "max_messages": NUM_MESSAGES},
            retry=retry.Retry(5),
    )
    if len(response.received_messages) == 0:
        return None, None

    return response.received_messages[0].message, response.received_messages[0].ack_id


def acknowledgment(ack_id):
    """ acknowledge PubSub that message being processed """
    subscriber.acknowledge(
            request={"subscription": subscription_path, "ack_ids": [ack_id]}
    )


def callback(message: google.cloud.pubsub_v1.types.PubsubMessage, ack_id: str) -> None:
    """ Function take retrieved PubSub message and acknowledgment for
     confirmation of retrieving"""
    data_from_message = json.loads(message.data)
    cat_id = data_from_message.get('cat_id')
    image_URL = data_from_message.get('imageForVideo')
    user_info = get_user_info(cat_id)
    if user_info:
        current_status = user_info.get('portraitVideoStatus')
        if current_status != 'processing2':
            change_status(cat_id, "processing")
            acknowledgment(ack_id)
            try:
                save_picture_for_converting_to_video(user_info, image_URL)
                convert_videos()
                links_and_types = upload_video_to_bucket(user_info, cat_id)
                change_status(cat_id, 'completed')
                add_links_for_video_to_cat(links_and_types, cat_id)
            except Exception as raised_error:
                change_status(cat_id, 'failed')
                save_error(cat_id, str(raised_error))
        else:
            acknowledgment(ack_id)
    else:
        acknowledgment(ack_id)


print('STARTING WATCHING PUBSUB')
while True:
    message_from_PubSub, ack_id = pull_messages()
    if message_from_PubSub:
        callback(message_from_PubSub, ack_id)
